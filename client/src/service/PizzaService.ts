import {IngredientDTO} from "../model/IngredientDTO";
import axios from 'axios';
import {PlacePizzaOrderDto} from "../model/PlacePizzaOrderDto";

export class PizzaService {

    private BASE_URL = "http://localhost:8080";

    async getIngredients(): Promise<Array<IngredientDTO>>{
        const response = await axios({
            method: "GET",
            baseURL: this.BASE_URL + "/ingredients",
            withCredentials: false,
            headers: {
                'Access-Control-Allow-Origin' : '*',
                'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
            },
            data: {}
        })

        return response.data
    }

    async placePizzaOrder(request: PlacePizzaOrderDto): Promise<number>{
        const response = await axios({
            method: "POST",
            baseURL: this.BASE_URL + "/orders/place",
            withCredentials: false,
            headers: {
                'Access-Control-Allow-Origin' : '*',
                'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',
            },
            data: request
        })

        return response.data
    }

}