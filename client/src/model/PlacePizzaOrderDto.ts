import {IngredientDTO} from "./IngredientDTO";

export interface PlacePizzaOrderDto {
    pizzaDTOList : {
        ingredientsDTO: Array<IngredientDTO>
    }
}