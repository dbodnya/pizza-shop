
export interface IngredientDTO{
    id: number | never;
    name: string;
    quantity: number;
}