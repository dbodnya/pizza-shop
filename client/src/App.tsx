import React, {ReactPropTypes} from 'react';
import './App.css';
import {Button, Checkbox, Container, Form, Header, Input, Segment} from "semantic-ui-react";
import {PizzaService} from "./service/PizzaService";
import {IngredientDTO} from "./model/IngredientDTO";
import {PlacePizzaOrderDto} from "./model/PlacePizzaOrderDto";


interface PizzaAppState {
    isOkButtonClicked: boolean;
    ingredientsInStock: Array<IngredientDTO>;
    isAnyCheckboxChecked: boolean;
    checked: [];
    ingredientsPicked: Array<IngredientDTO>
}

export default class PizzaApp extends React.Component {
    private _pizzaService = new PizzaService();

    // eslint-disable-next-line @typescript-eslint/no-useless-constructor
    constructor(props: ReactPropTypes) {
        super(props);

    }

    state: PizzaAppState = {
        ingredientsPicked: [],
        checked: [],
        isOkButtonClicked: false,
        ingredientsInStock: [],
        isAnyCheckboxChecked: false
    }

    async componentDidMount(): Promise<void> {
        const ingredientsInStock = await this._pizzaService.getIngredients();
        this.setState({ingredientsInStock})
    }

    async handleFormSubmit(e: React.FormEvent<HTMLFormElement>): Promise<void> {
        e.preventDefault();
        return Promise.resolve(undefined);
    }

    render(): React.ReactNode {
        const {isOkButtonClicked, ingredientsInStock, isAnyCheckboxChecked} = this.state;

        if (isOkButtonClicked) {
            return (
                <Container className='content-container' style={{
                    marginTop: 200,
                    marginLeft: 250
                }}>
                    <Header>Please pick ingredients:</Header>
                    <Form size='large'>
                        <Segment>
                            {
                                ingredientsInStock.sort(function (a, b) {
                                    return a.id - b.id
                                }).map((ingredient: IngredientDTO) =>
                                    <div key={ingredient.id}>
                                        <Checkbox
                                            onChange={(): void => this.onCheckboxChange(ingredient.id)}
                                            checked={this.state.checked.includes(ingredient.id as never)}
                                            key={ingredient.id}
                                            label={{children: ingredient.name}}/>
                                        {
                                            this.state.checked.includes(ingredient.id as never) ?
                                                <Input
                                                    onChange={(event): void => this.onEnterItemQuantity(event, ingredient)}
                                                    placeholder="Enter item quantity..."
                                                    style={{
                                                        marginLeft: 10
                                                    }}/> : null
                                        }
                                        <br/>
                                    </div>
                                )
                            }
                        </Segment>
                        {
                            isAnyCheckboxChecked ?
                                <Button primary onClick={(event): Promise<void> => this.placeOrder(event)}>Order
                                    Pizza</Button> : null
                        }
                    </Form>
                </Container>
            )
        }

        return (
            <Container className='content-container' style={{
                marginTop: 200,
                marginLeft: 250
            }}>
                <Header>Please place pizza order:</Header>
                <Button primary onClick={(): void => this.setState({isOkButtonClicked: true})}>OK</Button>
            </Container>
        )
    }

    onCheckboxChange(id: number): void {
        let checked = this.state.checked
        let find = checked.indexOf(id as never)

        if (find > -1) {
            checked.splice(find, 1)
        } else {
            checked.push(id as never);
        }
        this.setState({checked})
        if (this.state.checked.length) {
            this.setState({isAnyCheckboxChecked: true})
        } else {
            this.setState({isAnyCheckboxChecked: false})
        }
    }

    onEnterItemQuantity(e: React.ChangeEvent<HTMLInputElement>, ingredient: IngredientDTO): void {
        const re = /^[0-9\b]+$/;
        let ingredientsPicked = [...this.state.ingredientsPicked];
        const valid = e.target.value.trim().match(re);

        ingredientsPicked[ingredient.id] = {
            id: ingredient.id,
            name: ingredient.name,
            quantity: e.target.value as unknown as number

        }
        this.setState({ingredientsPicked});
    }


    async placeOrder(event: React.MouseEvent<HTMLButtonElement>): Promise<void> {
        event.preventDefault();
        let sortedIngredients = this.state.ingredientsPicked.filter(i => i !== undefined)
        let response;

        const request = {
            pizzaDTOList: [
                {
                    ingredientsDTO: sortedIngredients
                }
            ]
        }
        try {
            response =  await this._pizzaService.placePizzaOrder(request as unknown as PlacePizzaOrderDto);
        }catch (e: any) {
            window.alert(e.response.data)
        }

        if(response){
            window.alert("You successfully placed the order!!!")
            this.setState({isOkButtonClicked: false});
        }
    }
}


