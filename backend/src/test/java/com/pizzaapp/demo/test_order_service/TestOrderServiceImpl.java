package com.pizzaapp.demo.test_order_service;

import com.pizzaapp.demo.service.dto.OrderStatus;
import com.pizzaapp.demo.service.dto.PizzaOrderDTO;
import com.pizzaapp.demo.service.dto.PizzaOvenDTO;
import java.time.Duration;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


public class TestOrderServiceImpl {

  @Test
  public void anything() throws ExecutionException, InterruptedException {
    int bakingTime = 5;
    PizzaOrderDTO pizzaOrderDTO = new PizzaOrderDTO();
    pizzaOrderDTO.setId(1);
    pizzaOrderDTO.setOrderStatus(OrderStatus.RECEIVED);

    Flux.fromStream(IntStream.rangeClosed(1, bakingTime).boxed())
        .delayElements(Duration.ofSeconds(1))
        .doOnNext(s -> pizzaOrderDTO.setOrderStatus(OrderStatus.PROCESSING))
        .doOnComplete(() -> pizzaOrderDTO.setOrderStatus(OrderStatus.COMPLETED))
        .toStream()
        .collect(Collectors.toList())
        .forEach(System.out::println);
    System.out.println(pizzaOrderDTO.getOrderStatus());


   ///

    PizzaOrderDTO pizzaOrderDTO2 = new PizzaOrderDTO();
    pizzaOrderDTO2.setId(2);
    pizzaOrderDTO2.setOrderStatus(OrderStatus.PROCESSING);

    Mono.just("")
        .delayElement(Duration.ofSeconds(bakingTime))
        .doOnSuccess(e -> pizzaOrderDTO2.setOrderStatus(OrderStatus.COMPLETED))
        .toFuture().get();

    System.out.println(pizzaOrderDTO2.getOrderStatus());
  }

  @Test
  public void newTest() throws ExecutionException, InterruptedException {
    PizzaOrderDTO pizzaOrderDTO = new PizzaOrderDTO();
    pizzaOrderDTO.setId(1);
    pizzaOrderDTO.setOrderStatus(OrderStatus.RECEIVED);

    PizzaOvenDTO pizzaOvenDTO = new PizzaOvenDTO(1L);


     Mono.just("")
        .delayElement(Duration.ofSeconds(6))
        .toFuture().get();

    System.out.println(pizzaOrderDTO.getOrderStatus());
  }
}
