package com.pizzaapp.demo.service;

import com.pizzaapp.demo.service.dto.IngredientsDTO;
import com.pizzaapp.demo.service.dto.PizzaOrderDTO;
import java.util.List;

public interface IngredientService {

 void loadIngredients() throws Exception;

  List<IngredientsDTO> getIngredients();

  void  useIngredientsForPizza(PizzaOrderDTO pizzaOrderDTO) ;

}
