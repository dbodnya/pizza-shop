package com.pizzaapp.demo.service;

import com.pizzaapp.demo.service.dto.PizzaOrderDTO;
import reactor.core.publisher.Mono;

public interface OrderService {

  Mono<Long> placePizzaOrder(PizzaOrderDTO pizzaOrderDTO);
  Mono<PizzaOrderDTO> getPizzaOrderInfo(long orderId);
}
