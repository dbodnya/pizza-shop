package com.pizzaapp.demo.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import lombok.Data;

@Data
public class PizzaOrderDTO {

  private long id;
  private OrderStatus orderStatus = OrderStatus.RECEIVED;
  private List<PizzaDTO> pizzaDTOList;

  public synchronized void triggerOrderStatusUpdate() {
    orderStatus = OrderStatus.PROCESSING;
    int sum = pizzaDTOList.parallelStream()
        .mapToInt(PizzaDTO::getBakingTimeLeft)
        .sum();
    if(sum == 0){
      orderStatus = OrderStatus.COMPLETED;
    }
  }

  public void setId(long id) {
    this.id = id;
    pizzaDTOList.forEach(pizzaDTO -> pizzaDTO.setOrderId(id));
  }

}
