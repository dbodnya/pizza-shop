package com.pizzaapp.demo.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IngredientsDTO {

  private Integer id;
  private String name;
  private Integer quantity;


}
