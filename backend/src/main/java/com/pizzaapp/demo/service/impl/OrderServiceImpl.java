package com.pizzaapp.demo.service.impl;

import com.pizzaapp.demo.service.IngredientService;
import com.pizzaapp.demo.service.dto.PizzaOvenDTO;
import com.pizzaapp.demo.service.dto.OrderStatus;
import com.pizzaapp.demo.service.dto.PizzaDTO;
import com.pizzaapp.demo.service.dto.PizzaOrderDTO;
import com.pizzaapp.demo.service.OrderService;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicLong;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;


@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

  private ConcurrentHashMap<Long, PizzaOrderDTO> orders = new ConcurrentHashMap<>();
  private List<PizzaOvenDTO> ovens = Arrays.asList(new PizzaOvenDTO(1L), new PizzaOvenDTO(2L));
  private AtomicLong orderIdCount = new AtomicLong(1);
  private AtomicLong pizzaIdCount = new AtomicLong(1);
  private ConcurrentLinkedQueue<PizzaDTO> pizzasWaiting = new ConcurrentLinkedQueue<>();


  private final IngredientService ingredientService;

  @Autowired
  public OrderServiceImpl(IngredientService ingredientService) {
    this.ingredientService = ingredientService;
  }


  @Override
  public Mono<Long> placePizzaOrder(PizzaOrderDTO pizzaOrderDTO)  {
    ingredientService.useIngredientsForPizza(pizzaOrderDTO);

    long newOrderID = orderIdCount.getAndIncrement();
    pizzaOrderDTO.getPizzaDTOList().forEach(pizzaDTO -> {
      long newPizzaId = pizzaIdCount.getAndIncrement();
      pizzaDTO.setId(newPizzaId);
    });

    pizzaOrderDTO.setOrderStatus(OrderStatus.RECEIVED);
    pizzaOrderDTO.setId(newOrderID);
    assignPizzaOrder(pizzaOrderDTO);
    orders.put(newOrderID, pizzaOrderDTO);
    return Mono.just(newOrderID);
  }

  @Override
  public Mono<PizzaOrderDTO> getPizzaOrderInfo(long orderId) {
    PizzaOrderDTO pizzaOrderDTO = orders.get(orderId);
    if (pizzaOrderDTO == null) {
      pizzaOrderDTO = new PizzaOrderDTO();
      pizzaOrderDTO.setOrderStatus(OrderStatus.NOT_FOUND);
      pizzaOrderDTO.setId(orderId);
    }
    return Mono.just(pizzaOrderDTO);
  }

  private void submitNextOrder(PizzaOvenDTO pizzaOvenDTO) {
    PizzaDTO nextPizzaDTO = pizzasWaiting.poll();

    if (nextPizzaDTO == null) {
      return;
    }
    log.info("assigning pizza order {} to Oven {}", nextPizzaDTO.getId(), pizzaOvenDTO.getId());
    PizzaOrderDTO nextOrder = orders.get(nextPizzaDTO.getOrderId());
    pizzaOvenDTO.startBaking(nextPizzaDTO, nextOrder)
        .doFinally(p -> submitNextOrder(pizzaOvenDTO))
        .subscribe();
  }

  private void assignPizzaOrder(PizzaOrderDTO pizzaOrderDTO) {
    // Parallel stream will give us pseudo random assignment of ovens
    orders.put(pizzaOrderDTO.getId(), pizzaOrderDTO);
    pizzasWaiting.addAll(pizzaOrderDTO.getPizzaDTOList());
    ovens.parallelStream()
        .filter(PizzaOvenDTO::isFree)
        .forEach(this::submitNextOrder);
  }
}
