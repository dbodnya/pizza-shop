package com.pizzaapp.demo.service.dto;
import java.time.Duration;
import java.util.stream.IntStream;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;

@Data
@Slf4j
public class PizzaOvenDTO {

  private Long id;
  private final Integer bakingTime = 15;
  private Integer bakingTimeLeft = 0;
  private final Boolean lock = false;


  public PizzaOvenDTO(Long id) {
    this.id = id;
  }

  public Flux<Integer> startBaking(PizzaDTO pizzaDTO, PizzaOrderDTO pizzaOrderDTO) {
    pizzaDTO.setBakingTimeLeft(bakingTime);
    pizzaOrderDTO.triggerOrderStatusUpdate();
    bakingTimeLeft = bakingTime;
    Flux<Integer> bakingFlux = Flux.fromStream(IntStream.rangeClosed(0, bakingTime).boxed())
        .delayElements(Duration.ofSeconds(1))
        .doOnNext(i -> {
          bakingTimeLeft = bakingTime - i;
          pizzaDTO.setBakingTimeLeft(bakingTimeLeft);
          log.info(String.valueOf(bakingTimeLeft));
        })
        .doOnComplete(pizzaOrderDTO::triggerOrderStatusUpdate);
    return bakingFlux;
  }

  public Boolean isFree(){
    return bakingTimeLeft == 0;
  }
}
