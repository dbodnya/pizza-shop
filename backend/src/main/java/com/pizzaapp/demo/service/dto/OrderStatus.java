package com.pizzaapp.demo.service.dto;

public enum OrderStatus {

  RECEIVED,
  PROCESSING,
  COMPLETED,
  NOT_FOUND
}
