package com.pizzaapp.demo.service.impl;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;
import com.pizzaapp.demo.domain.IngredientEntity;
import com.pizzaapp.demo.mapper.IngredientEntityMapper;
import com.pizzaapp.demo.service.IngredientService;
import com.pizzaapp.demo.service.dto.IngredientsDTO;
import com.pizzaapp.demo.service.dto.PizzaOrderDTO;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.springframework.stereotype.Service;

@Service
public class IngredientServiceImpl implements IngredientService {

  private final String FILE_NAME = "docs/Ingredients.csv";
  private List<IngredientEntity> ingredientsEntities = new ArrayList<>();

  public IngredientServiceImpl() {

  }

  @Override
  @PostConstruct
  public void loadIngredients() throws IOException, CsvException {

   CSVReader reader = new CSVReaderBuilder(new FileReader(FILE_NAME))
       .build();

    ingredientsEntities = reader.readAll().stream()
        .map(data -> {
          IngredientEntity ingredientEntity = new IngredientEntity();
          ingredientEntity.setId(Integer.valueOf(data[0]));
          ingredientEntity.setName(data[1]);
          ingredientEntity.setQuantity(Integer.valueOf(data[2]));
          return ingredientEntity;
        }).collect(Collectors.toList());
  }

  @Override
  public List<IngredientsDTO> getIngredients() {
     final IngredientEntityMapper ingredientEntityMapper = new IngredientEntityMapper();
    return ingredientsEntities.stream()
        .map(ingredientEntityMapper::map)
        .collect(Collectors.toList());
  }


  private static Predicate<IngredientEntity> missingIngredients(
      List<IngredientEntity> ingredientsInStock) {
    return ingredientFromOrder -> {

      Optional<IngredientEntity> matchingIngredientInStock = ingredientsInStock.stream()
          .filter(Objects::nonNull)
          .filter(i -> i.getName().equals(ingredientFromOrder.getName()))
          .findAny();

      if (!matchingIngredientInStock.isPresent()) {
        return true;
      }
      Integer requestedQuantity = ingredientFromOrder.getQuantity();
      Integer availableQuantity = matchingIngredientInStock.get().getQuantity();

      return requestedQuantity > availableQuantity;
    };
  }

  private static Consumer<IngredientEntity> subtractIngredients(
      List<IngredientEntity> ingredientInStock) {

    return ingredientFromOrder -> {
      Optional<IngredientEntity> matchingIngredientInStock = ingredientInStock.stream()
          .filter(Objects::nonNull)
          .filter(i -> i.getName().equals(ingredientFromOrder.getName()))
          .findAny();

      if (matchingIngredientInStock.isPresent()) {
        final IngredientEntity availableIngredientInStock = matchingIngredientInStock.get();
        Integer availableQuantity = availableIngredientInStock.getQuantity();
        Integer requestedQuantity = ingredientFromOrder.getQuantity();

        availableIngredientInStock
            .setQuantity(availableQuantity - requestedQuantity);
      }
    };
  }

  @Override
  public synchronized void useIngredientsForPizza(PizzaOrderDTO pizzaOrderDTO)  {
     final IngredientEntityMapper ingredientEntityMapper = new IngredientEntityMapper();
    List<IngredientEntity> ingredientsDTO = pizzaOrderDTO.getPizzaDTOList()
        .parallelStream()
        .flatMap(pizzaDTO -> pizzaDTO.getIngredientsDTO().stream())
        .map(ingredientEntityMapper::map)
        .collect(Collectors.toList());

    Optional<IngredientEntity> missingIngredient = ingredientsDTO.stream()
        .filter(missingIngredients(ingredientsEntities))
        .findAny();

    if (missingIngredient.isPresent()) {
      //TODO: ideally it should be some custom exception saying that certain ingredient is missing
      throw new RuntimeException("You can't proceed with the order because there are some ingredients missing!!!");
    }
    ingredientsDTO
        .forEach(subtractIngredients(ingredientsEntities));
  }

}
