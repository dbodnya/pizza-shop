package com.pizzaapp.demo.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import lombok.Data;

@Data
public class PizzaDTO {

  private Long id;
  @JsonIgnore
  private Long orderId;
  //NOTE:-1 = received
  // 0 = done
  // > 0 = isBaking
  private Integer bakingTimeLeft = -1;
  private List<IngredientsDTO> ingredientsDTO;
}
