package com.pizzaapp.demo.mapper;

import com.pizzaapp.demo.domain.IngredientEntity;
import com.pizzaapp.demo.service.dto.IngredientsDTO;
import org.springframework.stereotype.Component;

@Component
public class IngredientEntityMapper {

  public IngredientsDTO map(IngredientEntity ingredientEntity) {
    return new IngredientsDTO(ingredientEntity.getId(),
        ingredientEntity.getName(),
        ingredientEntity.getQuantity()
    );
  }

  public IngredientEntity map(IngredientsDTO ingredientsDTO) {
    return new IngredientEntity(ingredientsDTO.getId(),
        ingredientsDTO.getName(),
        ingredientsDTO.getQuantity()
    );
  }
}
