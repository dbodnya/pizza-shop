package com.pizzaapp.demo.domain;

import com.opencsv.bean.CsvBindByPosition;
import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class IngredientEntity {

  @CsvBindByPosition(position = 0)
  private Integer id;
  @CsvBindByPosition(position = 1)
  private String name;
  @CsvBindByPosition(position = 2)
  private Integer quantity;

}
