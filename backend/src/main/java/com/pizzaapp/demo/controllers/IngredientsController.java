package com.pizzaapp.demo.controllers;

import com.pizzaapp.demo.service.IngredientService;
import com.pizzaapp.demo.service.dto.IngredientsDTO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;


@RestController
@RequestMapping(value = "/ingredients", produces = MediaType.APPLICATION_JSON_VALUE)
public class IngredientsController {

  private final IngredientService ingredientService;


  @Autowired
  public IngredientsController(IngredientService ingredientService) {
    this.ingredientService = ingredientService;
  }

  @CrossOrigin(origins = {"http://localhost:3000"})
  @GetMapping()
  public Flux<IngredientsDTO> getIngredients() {
    List<IngredientsDTO> ingredients = ingredientService.getIngredients();
    return Flux.fromIterable(ingredients);
  }
}
