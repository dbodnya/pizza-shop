package com.pizzaapp.demo.controllers;

import com.pizzaapp.demo.service.dto.PizzaOrderDTO;
import com.pizzaapp.demo.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@CrossOrigin(origins = {"http://localhost:3000"})
@RestController
@Slf4j
@RequestMapping(value = "/orders", produces = MediaType.APPLICATION_JSON_VALUE)
public class OrdersController {

  private OrderService orderService;

  @Autowired
  public OrdersController(OrderService orderService) {
    this.orderService = orderService;
  }

  //TODO: ideally it should be dedicated layer where all Exception handlers for custom Exception will be sitting .

  @ExceptionHandler(RuntimeException.class)
  public ResponseEntity<String> handleRuntimeException(RuntimeException ex){
    log.error("This is the exception : {}" + ex);
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
  }


  @PostMapping(value = "/place", consumes = MediaType.APPLICATION_JSON_VALUE)
  public Mono<Long> placeOrder(@RequestBody(required = true) PizzaOrderDTO pizzaOrderDTO) {
    return orderService.placePizzaOrder(pizzaOrderDTO)
        .onErrorResume(Mono::error);
  }


  @GetMapping("/status/{id}")
  public Mono<PizzaOrderDTO> getOrderStatus(@PathVariable Long id) {
    return orderService.getPizzaOrderInfo(id)
        .onErrorResume(Mono::error);
  }
}
